$(function () {
    containermap();
    containerCharts();
    containerBarsChart();
    containerHorizontalBarChart();
})

$("#competitorsAnchor").click(function () {
    $("#filterMenu").css({ display: "none" });
    $("#toggleFilterMenu").toggleClass('col-xs-4', false);
    $("#toggleFilterMenu").toggleClass('col-xs-4 col-md-4', false);
    $("#toggleFullScreen").toggleClass('col-xs-8', false);
    containermapCompetitorContent();
    containerChartsCompetitorContent();
    containerBarsChartCompetitorContent();
    containerHorizontalBarChartCompetitorContent();

});

$("#productAnchor").click(function () {
    $("#filterMenu").css({ display: "block" });
    $("#toggleFilterMenu").toggleClass('col-xs-4', true);
    $("#toggleFilterMenu").toggleClass('col-xs-4 col-md-4', true);
    $("#toggleFullScreen").toggleClass('col-xs-8', true);
    containermap();
    containerCharts();
    containerBarsChart();
    containerHorizontalBarChart();
});

$("#cust_accountsAnchor").click(function () {
    $("#filterMenu").css({ display: "block" });
    $("#toggleFilterMenu").toggleClass('col-xs-4', true);
    $("#toggleFilterMenu").toggleClass('col-xs-4 col-md-4', true);
    $("#toggleFullScreen").toggleClass('col-xs-8', true);
    var tbody = $('#customerTable tbody'), props = ["name", "name2"];
    tbody.empty();
    $.each([{ name: "DM", name2: "DM2" }, { name: "DM", name2: "DM2" }, { name: "DM", name2: "DM2" }, { name: "DM", name2: "DM2" }], function (i, item) {
        var tr = $('<tr>');
        $.each(props, function (i, prop) {
            $('<td>').html(item[prop]).appendTo(tr);
        });
        tbody.append(tr);
    });

    containermapNTP();
    containerChartsNTP();
    containerBarsChartNTP();
    containerHorizontalBarChartNTP();
});

$('#_revenue').slider({
    formatter: function (value) {
        return 'Current value: ' + value;
    }
});

$('#_employee').slider({
    formatter: function (value) {
        return 'Current value: ' + value;
    }
});

$('#emea').click(function () {
    $('#containermap').empty();
    $('#containermap').JSMaps({
        map: 'usaCanada'
    });
});

$('#productContact1').click(function() {
    // alert('contact data');
});

$('#productAccounts1').click(function() {
    // alert('accounts data');
});

$('.btn-toggle').click(function() {
    $(this).find('.btn').toggleClass('active');  
    
    if ($(this).find('.btn-primary').size()>0) {
    	$(this).find('.btn').toggleClass('btn-primary');
    }
    if ($(this).find('.btn-danger').size()>0) {
    	$(this).find('.btn').toggleClass('btn-danger');
    }
    if ($(this).find('.btn-success').size()>0) {
    	$(this).find('.btn').toggleClass('btn-success');
    }
    if ($(this).find('.btn-info').size()>0) {
    	$(this).find('.btn').toggleClass('btn-info');
    }
    
    $(this).find('.btn').toggleClass('btn-default');
       
});