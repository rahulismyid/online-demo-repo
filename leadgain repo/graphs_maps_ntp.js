var countryData = [
    { "Afghanistan": "AF" },
    { "Aland Islands": "AX" },
    { "Albania": "AL" },
    { "Algeria": "DZ" },
    { "American Samoa": "AS" },
    { "Andorra": "AD" },
    { "Angola": "AO" },
    { "Anguilla": "AI" },
    { "Antarctica": "AQ" },
    { "Antigua and Barbuda": "AG" },
    { "Argentina": "AR" },
    { "Armenia": "AM" },
    { "Aruba": "AW" },
    { "Australia": "AU" },
    { "Austria": "AT" },
    { "Azerbaijan": "AZ" },
    { "Bahamas the": "BS" },
    { "Bahrain": "BH" },
    { "Bangladesh": "BD" },
    { "Barbados": "BB" },
    { "Belarus": "BY" },
    { "Belgium": "BE" },
    { "Belize": "BZ" },
    { "Benin": "BJ" },
    { "Bermuda": "BM" },
    { "Bhutan": "BT" },
    { "Bolivia": "BO" },
    { "Bosnia and Herzegovina": "BA" },
    { "Bosnia and Herzegowina": "BA" },
    { "Botswana": "BW" },
    { "Bouvet Island (Bouvetoya)": "BV" },
    { "Brazil": "BR" },
    { "British Indian Ocean Territory (Chagos Archipelago)": "IO" },
    { "British Virgin Islands": "VG" },
    { "Brunei Darussalam": "BN" },
    { "Bulgaria": "BG" },
    { "Burkina Faso": "BF" },
    { "Burundi": "BI" },
    { "Cambodia": "KH" },
    { "Cameroon": "CM" },
    { "Canada": "CA" },
    { "Cape Verde": "CV" },
    { "Cayman Islands": "KY" },
    { "Central African Republic": "CF" },
    { "Chad": "TD" },
    { "Chile": "CL" },
    { "China": "CN" },
    { "Christmas Island": "CX" },
    { "Cocos (Keeling) Islands": "CC" },
    { "Colombia": "CO" },
    { "Comoros the": "KM" },
    { "Congo": "CD" },
    { "Congo the": "CG" },
    { "Cook Islands": "CK" },
    { "Costa Rica": "CR" },
    { "Cote d'Ivoire": "CI" },
    { "Croatia": "HR" },
    { "Cuba": "CU" },
    { "Cyprus": "CY" },
    { "Czech Republic": "CZ" },
    { "Denmark": "DK" },
    { "Democratic Republic of the Congo": "CD" },
    { "Djibouti": "DJ" },
    { "Dominica": "DM" },
    { "Dominican Republic": "DO" },
    { "Ecuador": "EC" },
    { "Egypt": "EG" },
    { "El Salvador": "SV" },
    { "Equatorial Guinea": "GQ" },
    { "Eritrea": "ER" },
    { "Estonia": "EE" },
    { "Ethiopia": "ET" },
    { "Faroe Islands": "FO" },
    { "Falkland Islands (Malvinas)": "FK" },
    { "Fiji": "FJ" },
    { "Fiji the Fiji Islands": "FJ" },
    { "Finland": "FI" },
    { "France": "FR" },
    { "France} French Republic": "FR" },
    { "French Guiana": "GF" },
    { "French Polynesia": "PF" },
    { "French Southern Territories": "TF" },
    { "Gabon": "GA" },
    { "Gambia the": "GM" },
    { "Georgia": "GE" },
    { "Germany": "DE" },
    { "Ghana": "GH" },
    { "Gibraltar": "GI" },
    { "Greece": "GR" },
    { "Greenland": "GL" },
    { "Grenada": "GD" },
    { "Guadeloupe": "GP" },
    { "Guam": "GU" },
    { "Guatemala": "GT" },
    { "Guernsey": "GG" },
    { "Guinea": "GN" },
    { "Guinea-Bissau": "GW" },
    { "Guyana": "GY" },
    { "Haiti": "HT" },
    { "Heard Island and McDonald Islands": "HM" },
    { "Holy See (Vatican City State)": "VA" },
    { "Honduras": "HN" },
    { "Hong Kong": "HK" },
    { "Hungary": "HU" },
    { "Iceland": "IS" },
    { "India": "IN" },
    { "Indonesia": "ID" },
    { "Iran": "IR" },
    { "Islamic Republic of Iran": "IR" },
    { "Iraq": "IQ" },
    { "Ireland": "IE" },
    { "Isle of Man": "IM" },
    { "Israel": "IL" },
    { "Italy": "IT" },
    { "Jamaica": "JM" },
    { "Japan": "JP" },
    { "Jersey": "JE" },
    { "Jordan": "JO" },
    { "Kazakhstan": "KZ" },
    { "Kenya": "KE" },
    { "Kiribati": "KI" },
    { "North Korea": "KP" },
    { "Korea (North)": "KP" },
    { "South Korea": "KR" },
    { "Korea (South)": "KR" },
    { "Kuwait": "KW" },
    { "Kyrgyz Republic": "KG" },
    { "Kyrgyzstan": "KG" },
    { "Lao": "LA" },
    { "Latvia": "LV" },
    { "Lebanon": "LB" },
    { "Lesotho": "LS" },
    { "Liberia": "LR" },
    { "Libyan Arab Jamahiriya": "LY" },
    { "Liechtenstein": "LI" },
    { "Lithuania": "LT" },
    { "Luxembourg": "LU" },
    { "Macao": "MO" },
    { "Macedonia": "MK" },
    { "Madagascar": "MG" },
    { "Malawi": "MW" },
    { "Malaysia": "MY" },
    { "Maldives": "MV" },
    { "Mali": "ML" },
    { "Malta": "MT" },
    { "Marshall Islands": "MH" },
    { "Martinique": "MQ" },
    { "Mauritania": "MR" },
    { "Mauritius": "MU" },
    { "Mayotte": "YT" },
    { "Mexico": "MX" },
    { "Micronesia": "FM" },
    { "Moldova": "MD" },
    { "Monaco": "MC" },
    { "Mongolia": "MN" },
    { "Montenegro": "ME" },
    { "Montserrat": "MS" },
    { "Morocco": "MA" },
    { "Mozambique": "MZ" },
    { "Myanmar": "MM" },
    { "Namibia": "NA" },
    { "Nauru": "NR" },
    { "Nepal": "NP" },
    { "Netherlands Antilles": "AN" },
    { "Netherlands": "NL" },
    { "New Caledonia": "NC" },
    { "New Zealand": "NZ" },
    { "Nicaragua": "NI" },
    { "Niger": "NE" },
    { "Nigeria": "NG" },
    { "Niue": "NU" },
    { "Norfolk Island": "NF" },
    { "Northern Mariana Islands": "MP" },
    { "Norway": "NO" },
    { "Oman": "OM" },
    { "Pakistan": "PK" },
    { "Palau": "PW" },
    { "Palestinian Territory": "PS" },
    { "Panama": "PA" },
    { "Papua New Guinea": "PG" },
    { "Paraguay": "PY" },
    { "Peru": "PE" },
    { "Philippines": "PH" },
    { "Pitcairn Islands": "PN" },
    { "Poland": "PL" },
    { "Portugal": "PT" },
    { "Portugal} Portuguese Republic": "PT" },
    { "Puerto Rico": "PR" },
    { "Qatar": "QA" },
    { "Reunion": "RE" },
    { "Romania": "RO" },
    { "Russian Federation": "RU" },
    { "Rwanda": "RW" },
    { "Saint Barthelemy": "BL" },
    { "Saint Helena": "SH" },
    { "Saint Kitts and Nevis": "KN" },
    { "Saint Lucia": "LC" },
    { "Saint Martin": "MF" },
    { "Saint Pierre and Miquelon": "PM" },
    { "Saint Vincent and the Grenadines": "VC" },
    { "Samoa": "WS" },
    { "San Marino": "SM" },
    { "Sao Tome and Principe": "ST" },
    { "Saudi Arabia": "SA" },
    { "Senegal": "SN" },
    { "Serbia": "RS" },
    { "Seychelles": "SC" },
    { "Sierra Leone": "SL" },
    { "Singapore": "SG" },
    { "Slovakia": "SK" },
    { "Slovakia (Slovak Republic)": "SK" },
    { "Slovenia": "SI" },
    { "Solomon Islands": "SB" },
    { "Somalia} Somali Republic": "SO" },
    { "South Africa": "ZA" },
    { "South Georgia and the South Sandwich Islands": "GS" },
    { "Spain": "ES" },
    { "Sri Lanka": "LK" },
    { "Sudan": "SD" },
    { "Suriname": "SR" },
    { "Svalbard & Jan Mayen Islands": "SJ" },
    { "Swaziland": "SZ" },
    { "Sweden": "SE" },
    { "Switzerland": "CH" },
    { "Switzerland} Swiss Confederation": "CH" },
    { "Syrian Arab Republic": "SY" },
    { "Taiwan": "TW" },
    { "Tajikistan": "TJ" },
    { "Tanzania": "TZ" },
    { "Thailand": "TH" },
    { "Timor-Leste": "TL" },
    { "Togo": "TG" },
    { "Tokelau": "TK" },
    { "Tonga": "TO" },
    { "Trinidad and Tobago": "TT" },
    { "Tunisia": "TN" },
    { "Turkey": "TR" },
    { "Turkmenistan": "TM" },
    { "Turks and Caicos Islands": "TC" },
    { "Tuvalu": "TV" },
    { "Uganda": "UG" },
    { "Ukraine": "UA" },
    { "United Arab Emirates": "AE" },
    { "United Kingdom": "GB" },
    { "United Kingdom (Great Britain)": "GB" },
    { "United States": "US" },
    { "United States of America": "US" },
    { "United States Minor Outlying Islands": "UM" },
    { "United States Virgin Islands": "VI" },
    { "Uruguay": "UY" },
    { "Uzbekistan": "UZ" },
    { "Vanuatu": "VU" },
    { "Venezuela": "VE" },
    { "Vietnam": "VN" },
    { "Wallis and Futuna": "WF" },
    { "Western Sahara": "EH" },
    { "Yemen": "YE" },
    { "Zambia": "ZM" },
    { "Zimbabwe": "ZW" }
];

function containermapNTP() {
    Highcharts.mapChart('containermapNTP', {
        title: {
            text: ''
        },
        legend: {
            title: {
                text: '',
                style: {
                    color: (Highcharts.theme && Highcharts.theme.textColor) || 'black'
                }
            }
        },
        mapNavigation: {
            enabled: true,
            buttonOptions: {
                verticalAlign: 'bottom'
            }
        },
        tooltip: {
            backgroundColor: 'none',
            borderWidth: 0,
            shadow: false,
            useHTML: true,
            padding: 0,
            pointFormat: '<span class="f32">' +
                '</span> {point.name}<br>' +
                '<span style="font-size:30px">{point.value}</span>',
            positioner: function () {
                return { x: 0, y: 250 };
            }
        },
        colorAxis: {
            min: 1,
            max: 1000,
            type: 'logarithmic'
        },
        series: [{
            data: countryData,
            mapData: Highcharts.maps['custom/world'],
            joinBy: ['iso-a2', 'code'],
            name: 'Domain Count',
            states: {
                hover: {
                    color: '#7cb5ec'
                }
            }
        }]
    });
}

function containerChartsNTP() {
    Highcharts.chart('containerChartsNTP', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Monthly Average Rainfall'
        },
        subtitle: {
            text: 'Source: WorldClimate.com'
        },
        xAxis: {
            categories: [
                'Jan',
                'Feb',
                'Mar',
                'Apr',
                'May',
                'Jun',
                'Jul',
                'Aug',
                'Sep',
                'Oct',
                'Nov',
                'Dec'
            ],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Rainfall (mm)'
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0
            }
        },
        series: [{
            name: 'Tokyo',
            data: [49.9, 71.5, 106.4, 129.2, 144.0, 176.0, 135.6, 148.5, 216.4, 194.1, 95.6, 54.4]

        }, {
            name: 'New York',
            data: [83.6, 78.8, 98.5, 93.4, 106.0, 84.5, 105.0, 104.3, 91.2, 83.5, 106.6, 92.3]

        }, {
            name: 'London',
            data: [48.9, 38.8, 39.3, 41.4, 47.0, 48.3, 59.0, 59.6, 52.4, 65.2, 59.3, 51.2]

        }, {
            name: 'Berlin',
            data: [42.4, 33.2, 34.5, 39.7, 52.6, 75.5, 57.4, 60.4, 47.6, 39.1, 46.8, 51.1]

        }]
    });
}

function containerBarsChartNTP() {
    Highcharts.chart('containerBarsChartNTP', {
        chart: {
            type: 'column'
        },
        title: {
            text: 'Browser market shares. January, 2018'
        },
        subtitle: {
            text: 'Click the columns to view versions. Source: <a href="http://statcounter.com" target="_blank">statcounter.com</a>'
        },
        xAxis: {
            type: 'category'
        },
        yAxis: {
            title: {
                text: 'Total percent market share'
            }

        },
        legend: {
            enabled: false
        },
        plotOptions: {
            series: {
                borderWidth: 0,
                dataLabels: {
                    enabled: true,
                    format: '{point.y:.1f}%'
                }
            }
        },

        tooltip: {
            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.2f}%</b> of total<br/>'
        },

        "series": [
            {
                "name": "Browsers",
                "colorByPoint": true,
                "data": [
                    {
                        "name": "Chrome",
                        "y": 62.74,
                        "drilldown": "Chrome"
                    },
                    {
                        "name": "Firefox",
                        "y": 10.57,
                        "drilldown": "Firefox"
                    },
                    {
                        "name": "Internet Explorer",
                        "y": 7.23,
                        "drilldown": "Internet Explorer"
                    },
                    {
                        "name": "Safari",
                        "y": 5.58,
                        "drilldown": "Safari"
                    },
                    {
                        "name": "Edge",
                        "y": 4.02,
                        "drilldown": "Edge"
                    },
                    {
                        "name": "Opera",
                        "y": 1.92,
                        "drilldown": "Opera"
                    },
                    {
                        "name": "Other",
                        "y": 7.62,
                        "drilldown": null
                    }
                ]
            }
        ],
        "drilldown": {
            "series": [
                {
                    "name": "Chrome",
                    "id": "Chrome",
                    "data": [
                        [
                            "v65.0",
                            0.1
                        ],
                        [
                            "v64.0",
                            1.3
                        ],
                        [
                            "v63.0",
                            53.02
                        ],
                        [
                            "v62.0",
                            1.4
                        ],
                        [
                            "v61.0",
                            0.88
                        ],
                        [
                            "v60.0",
                            0.56
                        ],
                        [
                            "v59.0",
                            0.45
                        ],
                        [
                            "v58.0",
                            0.49
                        ],
                        [
                            "v57.0",
                            0.32
                        ],
                        [
                            "v56.0",
                            0.29
                        ],
                        [
                            "v55.0",
                            0.79
                        ],
                        [
                            "v54.0",
                            0.18
                        ],
                        [
                            "v51.0",
                            0.13
                        ],
                        [
                            "v49.0",
                            2.16
                        ],
                        [
                            "v48.0",
                            0.13
                        ],
                        [
                            "v47.0",
                            0.11
                        ],
                        [
                            "v43.0",
                            0.17
                        ],
                        [
                            "v29.0",
                            0.26
                        ]
                    ]
                },
                {
                    "name": "Firefox",
                    "id": "Firefox",
                    "data": [
                        [
                            "v58.0",
                            1.02
                        ],
                        [
                            "v57.0",
                            7.36
                        ],
                        [
                            "v56.0",
                            0.35
                        ],
                        [
                            "v55.0",
                            0.11
                        ],
                        [
                            "v54.0",
                            0.1
                        ],
                        [
                            "v52.0",
                            0.95
                        ],
                        [
                            "v51.0",
                            0.15
                        ],
                        [
                            "v50.0",
                            0.1
                        ],
                        [
                            "v48.0",
                            0.31
                        ],
                        [
                            "v47.0",
                            0.12
                        ]
                    ]
                },
                {
                    "name": "Internet Explorer",
                    "id": "Internet Explorer",
                    "data": [
                        [
                            "v11.0",
                            6.2
                        ],
                        [
                            "v10.0",
                            0.29
                        ],
                        [
                            "v9.0",
                            0.27
                        ],
                        [
                            "v8.0",
                            0.47
                        ]
                    ]
                },
                {
                    "name": "Safari",
                    "id": "Safari",
                    "data": [
                        [
                            "v11.0",
                            3.39
                        ],
                        [
                            "v10.1",
                            0.96
                        ],
                        [
                            "v10.0",
                            0.36
                        ],
                        [
                            "v9.1",
                            0.54
                        ],
                        [
                            "v9.0",
                            0.13
                        ],
                        [
                            "v5.1",
                            0.2
                        ]
                    ]
                },
                {
                    "name": "Edge",
                    "id": "Edge",
                    "data": [
                        [
                            "v16",
                            2.6
                        ],
                        [
                            "v15",
                            0.92
                        ],
                        [
                            "v14",
                            0.4
                        ],
                        [
                            "v13",
                            0.1
                        ]
                    ]
                },
                {
                    "name": "Opera",
                    "id": "Opera",
                    "data": [
                        [
                            "v50.0",
                            0.96
                        ],
                        [
                            "v49.0",
                            0.82
                        ],
                        [
                            "v12.1",
                            0.14
                        ]
                    ]
                }
            ]
        }
    });
}

function containerHorizontalBarChartNTP() {
    // Data gathered from http://populationpyramid.net/germany/2015/
    // Age categories
    var categories = [
        '0-4', '5-9', '10-14', '15-19',
        '20-24', '25-29', '30-34', '35-39', '40-44',
        '45-49', '50-54', '55-59', '60-64', '65-69',
        '70-74', '75-79', '80-84', '85-89', '90-94',
        '95-99', '100 + '
    ];

    Highcharts.chart('containerHorizontalBarChartNTP', {
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Population pyramid for Germany, 2018'
        },
        subtitle: {
            text: 'Source: <a href="http://populationpyramid.net/germany/2018/">Population Pyramids of the World from 1950 to 2100</a>'
        },
        xAxis: [{
            categories: categories,
            reversed: false,
            labels: {
                step: 1
            }
        }],
        yAxis: {
            title: {
                text: null
            },
            labels: {
                formatter: function () {
                    return Math.abs(this.value) + '%';
                }
            }
        },

        plotOptions: {
            series: {
                stacking: 'normal'
            }
        },

        tooltip: {
            formatter: function () {
                return '<b>' + this.series.name + ', age ' + this.point.category + '</b><br/>' +
                    'Population: ' + Highcharts.numberFormat(Math.abs(this.point.y), 0);
            }
        },

        series: [{
            name: 'Female',
            data: [
                2.1, 2.0, 2.1, 2.3, 2.6,

            ]
        }]
    });
}
